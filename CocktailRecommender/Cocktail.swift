//
//  Cocktail.swift
//  CocktailRecommender
//
//  Created by Super Lesno Team: Stefano Di Nunno, Ioanna Stylianou, Ivan Tilev on 01/04/2020.
//  Copyright © 2020 Super Lesno Team: Stefano Di Nunno, Ioanna Stylianou, Ivan Tilev. All rights reserved.
//

// The Cocktail Model of the app, that every cocktail instance should have
struct Cocktail {
    
    var category: String
    
    var name: String
    
    var rating: Double
    
    init(category: String, name: String, rating: Double) {
        self.category = category
        self.name = name
        self.rating = rating
    }
}
