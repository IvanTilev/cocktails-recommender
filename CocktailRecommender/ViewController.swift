//
//  ViewController.swift
//  CocktailRecommender
//
//  Created by Super Lesno Team: Stefano Di Nunno, Ioanna Stylianou, Ivan Tilev on 01/04/2020.
//  Copyright © 2020 Super Lesno Team: Stefano Di Nunno, Ioanna Stylianou, Ivan Tilev. All rights reserved.
//
// This is supporting project following the article, about our Challenge at Apple Developer Academy
// We will show you the very basic idea and implementation of the model, that we created with CreateML early on.

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    // Label that will display to the used the cocktail that he picked from CocktailsTableViewController
    @IBOutlet weak var chosenCocktailLabel: UILabel!
    
    // TableView outlet, so we can implement da DataSource and Delegate to displey the top recommender cocktail
    @IBOutlet weak var tableView: UITableView!
    
    // Creating an instance of the model object, so we can work with the methods inside the class, that Xcode automatically generated for us.
    let model = CocktailModel1()
    
    // Will hold the users chosen cocktail, recieved from CocktailsTableViewController
    var cocktailName = String()
    
    var cocktailRating = Double()

    let cocktails = ["bloddy marry", "jack sprarrow"]
    
    var resultsBack: [String : Double] = [:]
    
    var recommenderCocktails: [String] = []
    
    var recommenderRatings: [Double] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.reloadData()
        
        // Assigning the fist result from the array to the label
        chosenCocktailLabel.text = cocktailName
        
        let user = [cocktailName : cocktailRating]
            
            // Creating an canstant of type CocktailModel1Input, that the model needs to make the recommendations
            let input = CocktailModel1Input(items: user, k: 5, restrict_: cocktails, exclude: cocktails)
            
            // Safely unwraping the prdiction that the model returns, because its optional and we need to assign it to a constant, that wll hold the results
            guard let unwrappedResults = try? model.prediction(input: input) else {
                fatalError("Could not get results back!")
            }
            
            // This constant will hold the .recommendations from the returned resuls from the model. They are in order from most relevant to least relevant
            let results = unwrappedResults.scores
        
            resultsBack = results
            
            print(results)
        
            print(cocktailName, cocktailRating)
        
        }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        for (name, rating) in resultsBack {
            recommenderCocktails.append(name)
            recommenderRatings.append(rating)
        }
    }
    
    // MARK: - Requred methods for the Data Source and Delegetion of the TableView:
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultsBack.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "recommendedCell", for: indexPath)
        
        cell.textLabel?.text = recommenderCocktails[indexPath.row]
        
        cell.detailTextLabel?.text = "Rating: \(recommenderRatings[indexPath.row])"
        
        return cell
    }

}

