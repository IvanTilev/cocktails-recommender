//
//  CocktailsTableViewController.swift
//  CocktailRecommender
//
//  Created by Super Lesno Team: Stefano Di Nunno, Ioanna Stylianou, Ivan Tilev on 01/04/2020.
//  Copyright © 2020 Super Lesno Team: Stefano Di Nunno, Ioanna Stylianou, Ivan Tilev. All rights reserved.
//

import UIKit

class CocktailsTableViewController: UITableViewController {
    
    // All cokctails data, that we will present in the table, so the user can picke his favorite one
    let sweetCocktails = [
        Cocktail(category: "sweet", name: "bellini", rating: 4),
        Cocktail(category: "sweet", name: "white russian", rating: 5),
        Cocktail(category: "sweet", name: "whiskey sour", rating: 3),
        Cocktail(category: "sweet", name: "amaretto sour", rating: 4),
        Cocktail(category: "sweet", name: "b-52", rating: 5),
        Cocktail(category: "sweet", name: "tequila sunrise", rating: 4),
        Cocktail(category: "sweet", name: "singapore sling", rating: 3),
        Cocktail(category: "sweet", name: "melon ball", rating: 3),
        Cocktail(category: "sweet", name: "irish coffee", rating: 4),
        Cocktail(category: "sweet", name: "gin fizz", rating: 3)
        ]
    
    let bitterCocktails = [
        Cocktail(category: "bitter", name: "negroni", rating: 3),
        Cocktail(category: "bitter", name: "aperol spritz", rating: 3),
        Cocktail(category: "bitter", name: "al capone", rating: 4),
        Cocktail(category: "bitter", name: "boulevardier", rating: 4),
        Cocktail(category: "bitter", name: "jungle bird", rating: 3),
        Cocktail(category: "bitter", name: "tequila sunrise", rating: 4),
        Cocktail(category: "bitter", name: "singapore sling", rating: 3),
        Cocktail(category: "bitter", name: "melon ball", rating: 3),
        Cocktail(category: "bitter", name: "irish coffee", rating: 4),
        Cocktail(category: "bitter", name: "gin fizz", rating: 3)
    ]
    
    let refreshingCocktails = [
        Cocktail(category: "refreshing", name: "daiquiri", rating: 5),
        Cocktail(category: "refreshing", name: "french 75", rating: 5),
        Cocktail(category: "refreshing", name: "moscow mule", rating: 4),
        Cocktail(category: "refreshing", name: "last word", rating: 4),
        Cocktail(category: "refreshing", name: "cosmo", rating: 3),
        Cocktail(category: "refreshing", name: "sherry cobler", rating: 3),
        Cocktail(category: "refreshing", name: "bellini", rating: 2),
        Cocktail(category: "refreshing", name: "aperol spritz", rating: 4)
        ]
    
    let spiritCocktails = [
        Cocktail(category: "spirit", name: "old fashioned", rating: 5),
        Cocktail(category: "spirit", name: "sazerac", rating: 5),
        Cocktail(category: "spirit", name: "brooklyn", rating: 5),
        Cocktail(category: "spirit", name: "bobby burns", rating: 5),
        Cocktail(category: "spirit", name: "manhattan", rating: 5),
        Cocktail(category: "spirit", name: "rob roy", rating: 4),
        Cocktail(category: "spirit", name: "negroni", rating: 4),
        Cocktail(category: "spirit", name: "irish coffee", rating: 4)
        ]
    
    let sourCocktails = [
        Cocktail(category: "sour", name: "pisco sour", rating: 5),
        Cocktail(category: "sour", name: "honeymoon", rating: 4),
        Cocktail(category: "sour", name: "hemingway daiquiri", rating: 3),
        Cocktail(category: "sour", name: "margarita", rating: 4)
    ]
        let fruityCocktails = [
        Cocktail(category: "fruity", name: "bellini", rating: 4),
        Cocktail(category: "fruity", name: "daiquiri", rating: 4),
        Cocktail(category: "fruity", name: "passion fruit martini", rating: 5),
        Cocktail(category: "fruity", name: "zombie", rating: 5),
        Cocktail(category: "fruity", name: "tequila sunrise", rating: 2),
        Cocktail(category: "fruity", name: "mimosa", rating: 4)
      ]
    
    var cocktails: [[Cocktail]] {
        return [sweetCocktails, bitterCocktails,refreshingCocktails, spiritCocktails, sourCocktails, fruityCocktails]
    }
    
    var userCocktail = Cocktail(category: "sweet", name: "bellini", rating: 4)
 
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source


    // Numbers of the section in the TablevIEW
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return cocktails.count
    }

    // Numbers of rows in each section of the TableView
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return cocktails[section].count
    }

    // Dequeue the reusable cell for the Tableview
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CocktailCell", for: indexPath)
        
        let cocktail = cocktails[indexPath.section][indexPath.row]
        
        cell.textLabel?.text = cocktail.name
        
        cell.detailTextLabel?.text = "Rating: \(cocktail.rating)"

        return cell
    }
    
    // Title for the Header of each Section:
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        let headerTitles = cocktails[section]
        
        if section < headerTitles.count {
            return headerTitles[section].category
        }
        
        return nil
    }
    
    // MARK: - Navigation
    
    // Did Select Row At:
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        userCocktail = cocktails[indexPath.section][indexPath.row]
        
        if let viewController = storyboard?.instantiateViewController(identifier: "toRecommender") as? ViewController {
            viewController.cocktailName = userCocktail.name
            viewController.cocktailRating = userCocktail.rating
            navigationController?.pushViewController(viewController, animated: true)
        }
    }

}
